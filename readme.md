# Projeto FLAG PHP #

## Passo-a-Passo para a instalação do projeto: ##

1- Clonar o projeto através do comando 
```
#!git

git clone https://bitbucket.org/flagphp/flag-news.git ProjectoPHP-DiogoOliveira
```


2- Através do terminal, entrar na pasta do projeto e dar um 
```
#!git

composer install
```

3- Depois disso, mudar o nome do ficheiro .env.example para .env e configurar as variáveis de desenvolvimento (tais como os dados de conexão para a base de dados).

4- Novamente no terminal, dar um 
```
#!git

php artisan key:generate
```
 para a criação de uma app key.

5- Criar uma base de dados com o nome dado no ficheiro .env e dar um import do ficheiro .sql enviado com o projeto na pasta public.

6- Finalmente, para correr o projeto no browser, dar um 
```
#!git

php artisan serve
```


## Estrutura do projeto ##

Como o projeto foi feito através da framework Laravel, temos muitas pastas e ficheiros que já vem de raiz. Para facilitar o entendimento do projeto, as pastas e ficheiros onde trabalhei foram:

* app/ (models: category, comments, news, newsletter)
* app/http/controllers (pastas: backoffice e site)
* app/http (ficheiro routes.php)
* resources/views (pastas: backoffice, site. ficheiros: layout.blade.php, site-layout.blade.php)
* public (ficheiro: style.css. pastas: other_components e images (com as imagens das noticias adicionadas))

## Navegação no site ##

localhost:8000 será a home page. Para acessar ao backoffice vá à url http://localhost:8000/backoffice (tem de criar uma conta no http://localhost:8000/register para ter acesso ao backoffice)