<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * Os atributos que devem ser mutated para datas.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Os atributos que são mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'description',
        'news_id'
    ];

    /**
     * Método da relação de um comentário com uma notícia (um comentário pertence a uma notícia).
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function news ()
    {
        return $this->belongsTo(News::class, 'news_id');
    }

    public function getCreatedAtAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $date)->diffForHumans();
    }
}
