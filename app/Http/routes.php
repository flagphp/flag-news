<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Grupo de rotas para o backoffice
Route::group(['prefix' => 'backoffice', 'as' => 'backoffice.', 'middleware' => 'auth'], function () {
    //Rota que mostra o dashboard
    Route::get('', function() {
        $title = "Dashboard";
       return view('backoffice.dashboard', compact('title'));
    });
    // Grupo de rotas para o CRUD de categorias no backoffice
    Route::group(['prefix' => 'categorias', 'as' => 'categories.', 'middleware' => 'web'], function () {
        Route::get('', ['as' => 'index', 'uses' => 'Backoffice\CategoryController@index']);
        Route::get('adicionar', ['as' => 'create', 'uses' => 'Backoffice\CategoryController@create']);
        Route::post('guardar', ['as' => 'store', 'uses' => 'Backoffice\CategoryController@store']);
        Route::get('editar/{id}', ['as' => 'edit', 'uses' => 'Backoffice\CategoryController@edit']);
        Route::post('alterar/{id}', ['as' => 'update', 'uses' => 'Backoffice\CategoryController@update']);
        Route::get('remover/{id}', ['as' => 'destroy', 'uses' => 'Backoffice\CategoryController@destroy']);
    });
    // Grupo de rotas para o CRUD de notícias no backoffice
    Route::group(['prefix' => 'noticias', 'as' => 'news.', 'middleware' => 'web'], function () {
        Route::get('', ['as' => 'index', 'uses' => 'Backoffice\NewsController@index']);
        Route::get('adicionar', ['as' => 'create', 'uses' => 'Backoffice\NewsController@create']);
        Route::post('guardar', ['as' => 'store', 'uses' => 'Backoffice\NewsController@store']);
        Route::get('editar/{id}', ['as' => 'edit', 'uses' => 'Backoffice\NewsController@edit']);
        Route::post('alterar/{id}', ['as' => 'update', 'uses' => 'Backoffice\NewsController@update']);
        Route::get('remover/{id}', ['as' => 'destroy', 'uses' => 'Backoffice\NewsController@destroy']);
    });
    // Grupo de rotas para banners
    Route::group(['prefix' => 'noticias/banner/{id}', 'as' => 'banner.', 'middleware' => 'web'], function () {
        Route::get('', ['as' => 'index', 'uses' => 'Backoffice\BannerController@index']);
        Route::post('guardar', ['as' => 'store', 'uses' => 'Backoffice\BannerController@store']);
        Route::get('remover', ['as' => 'destroy', 'uses' => 'Backoffice\BannerController@destroy']);
    });
});

// Grupo de rotas para o site
Route::group(['prefix' => '', 'as' => 'site.', 'middleware' => 'web'], function () {
    Route::get('', ['as' => 'home', 'uses' => 'Site\HomeController@index']);
    Route::post('newsletter', ['as' => 'newsletter', 'uses' => 'Site\NewsletterController@store']);
    Route::post('commentario/{news_id}', ['as' => 'comment', 'uses' => 'Site\CommentController@store']);
    Route::get('noticias/{category_id}/{category}', ['as' => 'index', 'uses' => 'Site\PageController@index']);
    Route::get('noticias/{category_id}/{category}/{slug}', ['as' => 'show', 'uses' => 'Site\PageController@show']);
});

Route::auth();
