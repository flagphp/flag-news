<?php

namespace App\Http\Controllers\Site;

use App\Category;
use App\Comment;
use App\News;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function index($category_id, $category)
    {
        $categories = Category::all();
        $category = Category::findOrFail($category_id);
        $title = "FLAG News | {$category->name}";
        $news = News::with('category')->where('category_id', '=', $category_id )->latest()->paginate(10);

        return view('site.page.index', compact('categories', 'category', 'news', 'title'));
    }

    public function show($category_id, $category, $slug)
    {
        $categories = Category::all();
        $category = Category::findOrFail($category_id);
        $news = News::with('category')->where('slug', '=', $slug)->first();
        $title = $news->title;
        $comments = Comment::where('news_id', '=', $news->id)->latest()->get();

        return view('site.page.show', compact('news', 'title', 'categories', 'category', 'comments'));
    }
}
