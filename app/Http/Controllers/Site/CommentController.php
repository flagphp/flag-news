<?php

namespace App\Http\Controllers\Site;

use App\Comment;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CommentController extends Controller
{
    public function store(Request $request, $news_id)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'description' => 'required|max:255'
        ]);

        Comment::create($request->all());

        return redirect()->back();
    }
}
