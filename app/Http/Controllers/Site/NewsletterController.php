<?php

namespace App\Http\Controllers\Site;

use App\Newsletter;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class NewsletterController extends Controller
{
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|email',
        ]);

        Newsletter::create($request->all());

        return redirect()->back();
    }
}
