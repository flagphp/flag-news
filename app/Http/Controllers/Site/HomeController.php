<?php

namespace App\Http\Controllers\Site;

use App\Category;
use App\News;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        $title = "FLAG News | Home";
        $categories = Category::all();
        $news = News::with('category')->latest()->take(10)->get();

        return view ('site.home.index', compact('title', 'categories', 'news'));
    }
}
