<?php

namespace App\Http\Controllers\Backoffice;

use App\News;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class BannerController extends Controller
{
    public function index($id)
    {
        $title = "Banner da Notícia";
        $news = News::findOrFail($id);

        $banner = !empty($news->banner) ? $news->banner : null;

        return view('backoffice.banner.index', compact('news', 'banner', 'title'));
    }

    public function store(Request $request, $id)
    {
        $image = $this->upload($request->file('banner'), $id);

        News::findOrFail($id)->update($image);

        return redirect()->back();
    }

    public function destroy($id)
    {
        $delete = ['banner' => null];

        $news = News::findOrFail($id);

        unlink(public_path("images/news/{$news->id}/{$news->banner}"));
        unlink(public_path("images/news/{$news->id}/thumbs/{$news->banner}"));
        rmdir(public_path("images/news/{$news->id}/thumbs"));
        rmdir(public_path("images/news/{$news->id}"));

        $news->update($delete);

        return redirect()->back();
    }

    public function upload($file, $id)
    {
        $image_path = public_path("images/news/{$id}");

        if (!is_dir($image_path . "/thumbs")) {
            mkdir($image_path . "/thumbs", 0777, true);
        }

        $news = News::findOrFail($id);

        $new_name = "{$news->slug}.{$file->getClientOriginalExtension()}";

        $image = Image::make($file)->save("{$image_path}/{$new_name}");

        $image->resize(600, 400, function ($constraint) {
            $constraint->aspectRatio();
        })->save("{$image_path}/thumbs/{$new_name}");

        return ['banner' => $new_name];
    }
}
