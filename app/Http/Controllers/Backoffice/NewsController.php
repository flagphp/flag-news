<?php

namespace App\Http\Controllers\Backoffice;

use App\Category;
use App\News;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Lista de Notícias";
        $news = News::with('category')->latest()->paginate(10);

        return view('backoffice.news.index', compact('news', 'title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Registo de Notícias";
        $categories = Category::lists('name', 'id');

        return view('backoffice.news.create', compact('title', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['slug'] = Str::slug($request['title']);
        $request['category_id'] = (empty($request['category_id']) ? null : $request['category_id']);

        $this->validate($request, [
            'title' => 'required|min:5',
            'summary' => 'required|max:255',
            'description' => 'required'
        ]);

        $news = News::create($request->all());

        return redirect()->route('backoffice.banner.index', $news->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edição de Notícias";
        $news = News::findOrFail($id);
        $categories = Category::lists('name', 'id');

        return view('backoffice.news.edit', compact('news', 'categories', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request['slug'] = Str::slug($request['title']);
        $request['category_id'] = (empty($request['category_id']) ? null : $request['category_id']);
        $this->validate($request, [
            'title' => 'required|min:5',
            'summary' => 'required|max:255',
            'description' => 'required',
            'category_id' => 'required'
        ]);

        News::findOrFail($id)->update($request->all());

        return redirect()->route('backoffice.banner.index', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        News::find($id)->delete();

        return redirect()->back();
    }
}
