<?php

namespace App\Http\Controllers\Backoffice;

use App\Category;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;

class CategoryController extends Controller
{
    /**
     * Mostra uma lista de categorias.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index ()
    {
        $title = "Lista das Categorias";
        $categories = Category::with('categoryFather')->latest()->paginate(10);

        return view('backoffice.categories.index', compact('categories', 'title'));
    }

    /**
     * Mostra o formulário para criar uma nova categoria.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create ()
    {
        $title = "Registo de Categorias";
        $categories = Category::lists('name', 'id');

        return view('backoffice.categories.create', compact('categories', 'title'));
    }

    /**
     * Guarda a nova categoria na base de dados.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store (Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:15',
        ]);

        $request['slug'] = Str::slug($request['name']);
        $request['category_id'] = (empty($request['category_id']) ? null : $request['category_id']);
        Category::create($request->all());

        return redirect()->route('backoffice.categories.index');
     }

    /**
     * Mostra o formulário para editar uma categoria específica.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit ($id)
    {
        $title = "Edição de Categorias";
        $category = Category::findOrFail($id);
        $categories = Category::where('id', '!=', $id)->get()->lists('name', 'id');

        return view('backoffice.categories.edit', compact('category', 'categories', 'title'));
    }

    /**
     * Atualiza uma categoria específica na base de dados.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update (Request $request, $id)
    {
        $request['slug'] = Str::slug($request['name']);
        $request['category_id'] = (empty($request['category_id']) ? null : $request['category_id']);

        $this->validate($request, [
            'name' => 'required|max:15'
        ]);

        Category::findOrFail($id)->update($request->all());

        return redirect()->route('backoffice.categories.index');
    }

    /**
     * Faz soft delete em uma cateforia específica.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy ($id)
    {
        Category::find($id)->delete();

        return redirect()->back();
    }
}
