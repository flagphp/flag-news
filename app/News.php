<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;

    /**
     * Os atributos que devem ser mutated para datas.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Os atributos que são mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'banner',
        'title',
        'slug',
        'summary',
        'description',
        'category_id'
    ];

    /**
     * Método da relação de uma notícia com uma categoria (uma notícia tem uma categoria).
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    /**
     * Método da relação de uma noticia com um comentario (uma noticia pode ter muitos comentários).
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments ()
    {
        return $this->hasMany(Comment::class, 'news_id', 'id');
    }

    public function getCreatedAtAttribute($date)
    {
        return ($this->attributes['created_at']) ? Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y H:i') : null;
    }

    public function getUpdatedAtAttribute($date)
    {
        return ($this->attributes['updated_at']) ? Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d/m/Y H:i') : null;
    }
}
