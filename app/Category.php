<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    /**
     * Os atributos que devem ser mutated para datas.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * Os atributos que são mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'category_id'
    ];

    /**
     * Método da relação de uma categoria com ela mesmo (uma categoria pode pertencer a outra categoria).
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categoryFather ()
    {
        return $this->belongsTo(Category::class, 'category_id');    
    }

    /**
     * Método da relação de uma categoria com ela mesmo (uma categoria pode ter muitas categorias).
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categoryChild()
    {
        return $this->hasMany(Category::class, 'category_id');
    }

    /**
     * Método da relação de uma categoria com uma notícia (uma categoria pode ter muitas notícias).
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function news()
    {
        return $this->hasMany(News::class, 'category_id', 'id');
    }
}
