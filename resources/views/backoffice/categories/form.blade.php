@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="col-xs-12 col-md-4 col-lg-4">
    <div class="form-group">
        {!! Form::label('name', 'Nome da Categoria') !!}
        {!! Form::text('name', null, ['class'=>'form-control']) !!}
    </div>
</div>

<div class="col-xs-12 col-md-4 col-lg-4">
    <div class="form-group">
        {!! Form::label('category_id', 'Categoria Pai') !!}
        {!! Form::select('category_id', $categories, null, ['class'=>'form-control', 'placeholder'=> 'Escolha...']) !!}
    </div>
</div>