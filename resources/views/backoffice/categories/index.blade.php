@extends('layout')

@section('content')
    <div class="row">
        <div class="col-xs-10">
            <h2>Categorias</h2>
        </div>
    </div>
    <hr>

    <a href="{{ route('backoffice.categories.create') }}" class="btn btn-primary">
        <i class="fa fa-plus"></i>
            &nbsp;Adicionar
    </a>
    <a href="{{ route('backoffice.') }}" class="btn btn-primary pull-right">
        <i class="fa fa-reply"></i>
        &nbsp;Voltar para o dashboard
    </a><br><br>

    <div class="table-list table-responsive">
        <table class="table table-striped table-hover">
            <thead>
                <th>Categoria</th>
                <th>Categoria Pai</th>
            </thead>
            <tbody>
            @forelse($categories as $category)
                <tr>
                    <td>{{ $category->name }}</td>
                    <td>{{ $category->categoryFather['name'] }}</td>
                    <td class="col-xs-2">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                Ações <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" style="min-width: initial;">
                                <li>
                                    <a href="{{route('backoffice.categories.edit', ['id' => $category->id])}}">Editar</a>
                                </li>
                                <li>
                                    <a href="{{route('backoffice.categories.destroy', ['id' => $category->id])}}">Excluir</a>
                                </li>
                            </ul>
                        </div>
                    </td>
                    @empty
                        <div class="alert alert-info">
                            <i class="fa fa-info-circle"></i>
                            &nbsp;Não há categorias adicionadas.
                        </div>
                    @endforelse
                </tr>
            </tbody>
        </table>
    </div>

@endsection