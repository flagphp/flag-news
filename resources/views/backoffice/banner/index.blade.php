@extends('layout')

@section('content')
    <div class="row">
        <div class="col-xs-10">
            <h2>Banner da Notícia</h2>
        </div>
    </div>
    <hr>

    <div>
        @if(empty($banner))
            <div class="alert alert-info">
                <p>Banner obrigatório</p>
            </div>
            {!! Form::open(['route' => ['backoffice.banner.store', $news->id], 'files' => true, 'id' => 'form-upload']) !!}
            <div class="form-group col-xs-4">
                <label>&nbsp;</label>
                <input type="file" id="banner" name="banner">
            </div>
            {!! Form::close() !!}
        @else
            <div class="alert alert-danger">
                <p>Esta notícia já possui um banner principal. Apague a mesma para que possa inserir uma nova</p>
            </div>
        @endif
    </div>

    <div>
        @if(!empty($banner))
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <a href="{{ route('backoffice.news.index') }}" class="btn btn-lg btn-primary">
                        <i class="fa fa-reply"></i>
                        &nbsp;Voltar a lista das notícias
                    </a>

                    <a href="{{ route('backoffice.banner.destroy', $news->id) }}" class="btn btn-danger btn-lg text-right">
                        <i class="fa fa-trash"></i>&nbsp;
                        Apagar
                    </a>
                </div>
            </div>

            <img class="img-responsive" src="{{ asset("/images/news/{$news->id}/{$banner}") }}" id="target">
        @endif
    </div>

@endsection

@section('js')
    <script>
        $(function () {
            $('#banner').on('change', function () {
                $('#form-upload').submit();
            });
        });
    </script>
@endsection