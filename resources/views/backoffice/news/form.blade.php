@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
        {!! Form::label('category_id', 'Categoria') !!}
        {!! Form::select('category_id', $categories, null, ['placeholder' => 'Selecione...','class' => 'form-control']) !!}
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-4">
    <div class="form-group">
        {!! Form::label('title', 'Título da Notícia') !!}
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
    </div>
</div>

@if(!empty($news))
    <div class="col-xs-12 col-sm-12 col-md-4">
        <div class="form-group">
            {!! Form::label('created_at', 'Data da Notícia') !!}
            {!! Form::text('created_at', null, ['class' => 'form-control', 'disabled']) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-4">
        <div class="form-group">
            {!! Form::label('updated_at', 'Última Atualização') !!}
            {!! Form::text('updated_at', null, ['class' => 'form-control', 'disabled']) !!}
        </div>
    </div>
@endif

<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
        {!! Form::label('summary', 'Resumo da Notícia') !!}
        {!! Form::textarea('summary', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-12">
    <div class="form-group">
        {!! Form::label('description', 'Texto') !!}
        {!! Form::textarea('description', null, ['class' => 'form-control ckeditor', 'id' => 'description']) !!}
    </div>
</div>

@section('js')
    <script src="{{ asset("/other_components/ckeditor/ckeditor.js") }}"></script>
    <script>
        CKEDITOR.replace('description',
                {
                    customConfig : 'config-default.js',
                    toolbarGroups: [
                        {"name":"document","groups":["mode"]},
                        {"name":"basicstyles","groups":["basicstyles"]},
                        {"name":"links","groups":["links"]},
                        {"name":"paragraph","groups":["list","blocks","align"]},
                        {"name":"insert","groups":["insert"]},
                        {"name":"styles","groups":["styles"]},
                    ],
                    removeButtons: 'Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
                }
        );
    </script>
@endsection