@extends('layout')

@section('content')
    <div class="row">
        <div class="col-xs-10">
            <h2>Editar Notícia</h2>
        </div>
    </div>
    <hr>

    <a href="{{ route('backoffice.news.index') }}" class="btn btn-primary">
        <i class="fa fa-reply"></i>
        &nbsp;Voltar
    </a><br><br>

    {!! Form::model($news, ['route'=>['backoffice.news.update', $news->id]]) !!}
        @include('backoffice.news.form')<br>
        <div class="col-xs-5 col-sm-4 col-md-3 col-lg-3">
            <button type="submit" class="btn btn-block btn-primary">
                <i class="fa fa-floppy-o"></i>
                &nbsp;Gravar
            </button>
        </div>
    {!! Form::close() !!}



@endsection