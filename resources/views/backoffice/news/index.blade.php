@extends('layout')

@section('content')
    <div class="row">
        <div class="col-xs-10">
            <h2>Notícias</h2>
        </div>
    </div>
    <hr>

    <a href="{{ route('backoffice.news.create') }}" class="btn btn-primary">
        <i class="fa fa-plus"></i>
            &nbsp;Adicionar
    </a>
    <a href="{{ route('backoffice.') }}" class="btn btn-primary pull-right">
        <i class="fa fa-reply"></i>
        &nbsp;Voltar para o dashboard
    </a><br><br>

    <div class="table-list table-responsive">
        <table class="table table-striped table-hover">
            <thead>
                <th>Título</th>
                <th>Resumo</th>
                <th>Categoria</th>
                <th>Criado em:</th>
                <th>Ultima atualização:</th>
            </thead>
            <tbody>
            @forelse($news as $new)
                <tr>
                    <td>{{ $new->title }}</td>
                    <td>{!! str_limit($new->summary, 60) !!}</td>
                    <td>{{ $new->category['name'] }}</td>
                    <td>{{ $new->created_at }}</td>
                    <td>{{ $new->updated_at }}</td>
                    <td class="col-xs-2">
                        <div class="btn-group">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                Ações <span class="caret"></span>
                            </button>

                            <ul class="dropdown-menu" role="menu" style="min-width: initial;">
                                <li>
                                    <a href="{{route('backoffice.news.edit', ['id' => $new->id])}}">Editar</a>
                                </li>
                                <li>
                                    <a href="{{route('backoffice.news.destroy', ['id' => $new->id])}}">Excluir</a>
                                </li>
                            </ul>
                        </div>
                    </td>
                    @empty
                        <div class="alert alert-info">
                            <i class="fa fa-info-circle"></i>
                            &nbsp;Não há notícias adicionadas.
                        </div>
                    @endforelse
                </tr>
            </tbody>
        </table>
        <div class="text-center">
            {{ $news->render() }}
        </div>
    </div>

@endsection