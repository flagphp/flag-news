@extends('layout')

@section('content')
    <div class="row">
        <div class="col-xs-10">
            <h2>Dashboard</h2>
        </div>
    </div>
    <hr>

    <section class="content">
        <div class="row row-btn">
            <div class="col-md-6 col-sm-6 col-xs-6">
                <a href="{{ route('backoffice.categories.index') }}" class="btn btn-primary btn-block btn-lg icon-dash">
                    <i class="fa fa-sitemap"></i><br>
                    <small>Categorias</small>
                </a>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-6">
                <a href="{{ route('backoffice.news.index') }}" class="btn btn-primary btn-block btn-lg icon-dash">
                    <i class="fa fa-file-text"></i><br>
                    <small>Notícias</small>
                </a>
            </div>
        </div>
    </section>
@endsection