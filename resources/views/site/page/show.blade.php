@extends('site-layout')

@section('content')
    <div id="header-wrapper" class="container">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 head-col">
            <div class="titlewrapper">
                <h1>
                    <a href="{{ route('site.index', [$category->id, $category->slug]) }}" title="Flag News">{{ $category->name }}</a>
                </h1>
            </div>
        </div>
    </div>

    <div class="main-wrapper">
        <div class="container main-container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main-post-col">
                    <div class="post-p-heading">
                        <h1 class="main-heading-post">
                            {{ $news->title }}
                        </h1>
                    </div>

                    <div class="post-meta-post">
        			    <span class="pull-left">
                            <span>
                      	        <a class="btn btn-info btn-sm" href="{{ route('site.index', [$category->id, $category->slug]) }}">{{ $category->name }}</a>
                            </span>
                        </span>
                    </div>

                    <div class="page-post">
                        <img class="clearfix img-responsive" alt="{{ $news->title }}" title="{{ $news->title }}" src="{{ asset("/images/news/{$news->id}/{$news->banner}") }}"/></a>
                    </div>

                    <div class="page-post page-description">
                        <p>{!! $news->description !!}</p>
                    </div>
                    <hr>

                    <span>
                        <a class="btn btn-info btn-sm" href="{{ route('site.index', [$category->id, $category->slug]) }}">{{ $category->name }}</a>
                    </span>
                    <hr>
                    @include('site.page.comment')
                </div>
            </div>
        </div>
    </div>

@endsection