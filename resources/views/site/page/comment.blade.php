@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="container">
    {!! Form::open(['route' => ['site.comment', $news->id]]) !!}
    <input type="hidden" name="news_id" value="{{$news->id}}">

    <div class="col-xs-12 col-md-6 col-lg-6">
        <div class="form-group">
            {!! Form::label('name', 'Nome') !!}
            {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Insira o seu nome']) !!}
        </div>
    </div>

    <div class="col-xs-12 col-md-6 col-lg-6">
        <div class="form-group">
            {!! Form::label('email', 'Email') !!}
            {!! Form::text('email', null, ['class'=>'form-control', 'placeholder'=> 'Insira o seu email']) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            {!! Form::label('description', 'Comentário') !!}
            {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder'=> 'Insira o seu comentário', 'rows' => '3']) !!}
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
        <div class="form-group">
            <button type="submit" class="btn btn-block btn-primary">
                <i class=" fa fa-paper-plane"></i>
                &nbsp;Submeter
            </button>
        </div>
    </div>
    {!! Form::close() !!}
</div>
<hr>

@forelse($comments as $comment)
<div class="container col-xs-12 col-sm-12 col-md-12 col-lg-12 comments">
    <div class="col-xs-12 col-md-10 col-lg-10">
        <strong>{{ $comment->name }}</strong>&nbsp;&nbsp;
        <small class="detail">{{ $comment->email }}</small>
    </div>
    <small class="col-xs-12 col-md-2 col-lg-2 detail">{{ $comment->created_at }}</small>
    <br><br>
    <div class="col-xs-12 col-md-12 col-lg-12">
        {{ $comment->description }}
    </div>
</div>
<hr>
@empty
<div class="alert alert-info">Esta notícia não tem comentários</div>
@endforelse
