@extends('site-layout')

@section('content')

    <div id="header-wrapper" class="container">
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 head-col">
            <div class="titlewrapper">
                <h1>
                    <a href="" title="Flag News">FLAG News</a>
                </h1>
            </div>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container newsletter">
        <div class="col-xs-12 col-md-12 col-lg-12">
            <h2 class="text-center">Newsletter</h2>
        </div>
        {!! Form::open(['route' => ['site.newsletter']]) !!}
        <div class="col-xs-12 col-md-4 col-lg-4">
            <div class="form-group">
                {!! Form::label('name', 'Nome') !!}
                {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Insira o seu nome']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-md-6 col-lg-6">
            <div class="form-group">
                {!! Form::label('email', 'Email') !!}
                {!! Form::text('email', null, ['class'=>'form-control', 'placeholder'=> 'Insira o seu email']) !!}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
            <div class="form-group">
                <label for="submit">&nbsp;</label>
                <button type="submit" class="btn btn-block btn-primary">
                    <i class=" fa fa-paper-plane"></i>
                    &nbsp;Submeter
                </button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>

    <div class="main-wrapper">
        <div class="container main-container">
            <div class="row main-index">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 main-post-col">
                    <div class="thumbnails">
                        @forelse($news as $new)
                            <div class="entry row">
                                <div class="post-content">
                                    <div class="col-lg-4 col-md-4">
                                        <a class="thumbnail ajax_link" title="{{ $new->title }}" href="{{ route('site.show', [$new->category['id'], $new->category['slug'], $new->slug]) }}">
                                            <img class="clearfix img-responsive" alt="{{ $new->title }}" title="{{ $new->title }}" src="{{ asset("/images/news/{$new->id}/thumbs/{$new->banner}") }}"/></a>
                                    </div>

                                    <div class="col-lg-8 col-md-8">
                                        <div class="content-row caption">
                                            <h2 class="post-title-h2"><a href="{{ route('site.show', [$new->category['id'], $new->category['slug'], $new->slug]) }}" title="{{ $new->title }}">
                                                    {{ $new->title }}
                                                </a>
                                            </h2>
                                        </div>

                                        <div>
                                            {{ $new->summary }}
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-md-12">
                                        <div class="post-meta-home">
                                            <span class="pull-left">
                                                <span class="date-meta meta-elements">
                                                     {{ $new->created_at }}
                                                </span>
                                                <span class="tag-meta meta-elements">
                                                    <a class="btn btn-success" href="{{ route('site.index', [$new->category['id'], $new->category['slug']]) }}">
                                                        {{ $new->category['name'] }}
                                                    </a>
                                                </span>
                                            </span>

                                            <span class="pull-right">
                                                <a class="btn btn-primary" href="{{ route('site.show', [$new->category['id'], $new->category['slug'], $new->slug]) }}">
                                                    Read More
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @empty
                        <div class="alert alert-info">Não existem notícias</div>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection