<!DOCTYPE html>
<html>

<head>
    <title>{{ $title or 'FLAG News' }}</title>
    {{-- Styles --}}
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    @yield('css')
    {{-- Fonts --}}
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>
<header class="navbar navbar-inverse navbar-fixed-top" role="banner">
    <div class="container">
        <div class="navbar-header">
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" title="Home" href="{{ route('site.home') }}">Home</a>
        </div>
        <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
            <ul class="nav navbar-nav">
                @foreach($categories as $category)
                    <li>
                        <a title="{{ $category->name }}" href="{{ route('site.index', [$category->id, $category->slug]) }}">{{ $category->name }}</a>
                    </li>
                @endforeach
            </ul>
        </nav>
    </div>
</header>

<div class="container">
    @yield('content')
</div>

<footer id="footer">
    <div class="container">
        <p class="footer-p">
            Copyright © 2016 FLAG News
        </p>
    </div>
</footer>
{{-- Javascript --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
@yield('js')
</body>

</html>
